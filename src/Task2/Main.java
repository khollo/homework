package Task2;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Write an application calculating BMI (Body Mass Index) and checking if itЀs optimal or not.
 * Your application should read two variables: weight (in kilograms, type float) and height
 * (in centimeters, type int). BMI should be calculated given following formula:
 * The optimal BMI range is from 18.5 to 24.9, smaller or larger values are non-optimal
 * values. Your program should write "BMI optimal" or "BMI not optimal", according to the
 * assumptions above.
 * <p>
 * BMI = weight[kg] / height[m]^2
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

            System.out.println("Weight: ");
            int weight = scanner.nextInt();
            System.out.println("Height: ");
            int height = scanner.nextInt();
            checkBMI(weight, height);
    }


    private static void checkBMI(float weight, int height) {
        float calculateBMI = (float) (weight / (Math.pow(height / 100F, 2)));
        System.out.println(calculateBMI <= 24.9 && calculateBMI >= 18.5 ? "BMI optimal" : "BMI not optimal");

    }

}
