package Task8;

import java.util.Scanner;

/**
 * Write an application that implements a simple calculator. The application should:a. read first number (type float)b.
 * read one of following symbols: + - / *c. read second number (type float)d. return a result of given mathematical
 * operation. If the user provides a symbol other than supported, the application should print "Invalid symbol".
 * If  the  entered  action  cannot  be  implemented  (i.e.  it  is  inconsistent  with  the principles of mathematics),
 * the application should print "Cannot calculate".
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter first number: ");
        float a = Float.parseFloat(scanner.nextLine());


        System.out.print("\nEnter operator: ");
        String input = scanner.nextLine();

        System.out.print("\nEnter second number: ");
        float b = scanner.nextFloat();

        if (!isTrue(input)) {
            System.out.println("Invalid symbol");
        } else if (isTrue(input)) {
            System.out.println(calculation(a, input, b));
        } else
            System.out.println("Cannot calculate");
    }

    

    public static float calculation(float a, String input, float b) {

        float sum = 0;

        if (isTrue(input))
            switch(input) {
                case "+":
                    sum = a + b;
                    break;
                case "-":
                    sum = a - b;
                    break;
                case "*":
                    sum = a * b;
                    break;
                case "/":
                    sum = a / b;
                    break;
            }
        return sum;


    }
    
    public static boolean isTrue(String input) {
        return (input.equals("+")) || (input.equals("-")) || (input.equals("*")) || (input.equals("/"));
    }

}
