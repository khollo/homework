package Task11;

import java.util.Scanner;

/**
 * Write an application that will read texts (variables of the String type) until the user gives
 * the text "Enough!" and then writes the longest of the given texts (not including the text
 * "Enough!"). If the user does not provide any text, write "No text provided".
 */

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
    stringBuilder();
    }

    private static void stringBuilder() {
      String sentence = "";
    while(true) {
        String scan = scanner.nextLine();
        if(scan.equals("Enough!")) {
            break;
        } else if (scan.equals("")){
            System.out.println("No text provided");
            break;
        } else {
            sentence += scan + " ";
        }
    }
        System.out.print(sentence);
    }
}
