package Task12;


import java.util.Scanner;

/**
 * Write an application that reads a text from the user (type String) and counts a percentage
 * of occurrences of a space character.
 */

public class Main {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    String text = scanner.nextLine();

        spaceCounter(text);
    }

    private static void spaceCounter(String text) {
        char space = ' ';
        double counter = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == space) {
                counter++;
            }
        }
        double percent = (counter / text.length()) * 100;
        System.out.println(percent + "%");

    }
}
