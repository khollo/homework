package Task7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        fibonacci(number);
    }
    public static void fibonacci(int number) {
        int a = 1;
        int b = 1;
        for(int i = 1; i <= number; i++) {
            System.out.println(a + " ");
            int sum = b + a;
            a = b;
            b = sum;
        }
    }
}
