package Task15;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Write an application that reads from the user 10 arbitrarily large numbers (variables of
 * type int) and prints those that occurred at least twice.
 */

public class Main {
    public static void main(String[] args) {
        checkOccurrence();
    }

    public static void checkOccurrence() {
        Scanner scanner = new Scanner(System.in);
        int[] list = new int[10];
        int i = 0;
        while(i < list.length) {
            System.out.println((i + 1)+ " Number: ");
            int input = scanner.nextInt();
            list[i] = input;
            i++;
        }

        for(int j = 0; j < list.length; j++) {
            for(int k = j + 1; k < list.length; k++) {
                if (list[j] == list[k]) {
                    System.out.println(list[k]);
                }
            }
        }

        System.out.println(Arrays.toString(list));
    }
}
