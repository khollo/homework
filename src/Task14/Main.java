package Task14;


/**
 * Write an application that reads two lowercase letters of the Latin alphabet (type char)
 * and calculates how many characters is there in the alphabet between given letters.
 * Hint - use the ASCII code table and treat the characters as int numbers.
 */

public class Main {
    public static void main(String[] args) {
    differenceCalculator('d','s');
    }

    private static void differenceCalculator(char a, char b) {
        int first = a;
        int second = b;

        int big = 0;
        int small = 0;

        if(first > second) {
            big = a;
            small = b;
        } else {
            big = b;
            small = a;
        }

        System.out.println(big - small);
    }
}
