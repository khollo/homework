package Task16;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Write an application that takes 10 numbers from the user (type int) and write the length
 * of the longest such subsequence of these numbers, which is increasing.
 * For example, for the numbers: "1, 3, 8, 4, 2, 5, 6, 11, 13, 7" the program should write "5"
 * as the length of the longest increasing subsequence (underlined in the example).
 */

public class Main {
    public static void main(String[] args) {
        int[] list = {1,3,8,4,2,5,6,11,13,7};
    longestNumberSequence(list);

    }

    private static void longestNumberSequence(int[] list) {
        Scanner scanner = new Scanner(System.in);

//        for(int i = 0; i < list.length; i++) {
//            System.out.println("Please insert number " + (i + 1));
//            int input = scanner.nextInt();
//            list[i] = input;
//
//        }

        int counter = 0;
        for(int i = 0; i < list.length; i++) {
            if(list[i] < list[i + 1]) {
                System.out.println(list[i]);
            }
        }
        System.out.println(list[1]);
        System.out.println(counter);
        System.out.println(Arrays.toString(list));
    }
}


