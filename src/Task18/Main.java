package Task18;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Write an application that reads a text from the user (type String) and checks whether the user sneezed, i.e.
 * whether the text equals ‘achooo!’ with one or more letter "o" at the end of the expression
 * (so both 'acho!' and 'achooooooo!’ are correct). Hint: use a regular expression with the appropriate quantifier.
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert acho(n)!");
        String input = scanner.nextLine();
        System.out.println(checkForChoo(input));
    }


    private static boolean checkForChoo(String input) {
        Pattern patter = Pattern.compile("acho{1,999}!");
        if(Pattern.matches(String.valueOf(patter), input)) {
            return true;
        } return false;


    }

}
