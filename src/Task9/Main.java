package Task9;

import java.util.Scanner;

/**
 * Write an application that will take a positive number from the user (type int) and draw a
 * wave with a given length and height of 4 lines, according to the following example (fill
 * empty spaces with spaces):
 */

public class Main {
    public static void main(String[] args) {
        int waveSize = 4;
        for (int i = 0; i < waveSize; i++) System.out.print("*      *");
        System.out.println();
        for (int i = 0; i < waveSize; i++) System.out.print(" *    * ");
        System.out.println();
        for (int i = 0; i < waveSize; i++) System.out.print("  *  *  ");
        System.out.println();
        for (int i = 0; i < waveSize; i++) System.out.print("   **   ");
    }
}