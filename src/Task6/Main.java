package Task6;

import java.util.Scanner;

/**
 * Write an application that takes a number n from the user (type int) and calculates the sum of the harmonic series from 1 to n, according to the formula below:
 * Hn = 1 + 1/2 + 1/3 + 1/4 + 1/5 + 1/n...
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        harmonicNumber(number);
    }
    private static void harmonicNumber(int number) {
        double sum = 0;
        for(int i = 1; i <= number; i++) {
            sum += 1.0 / i;
        }
        System.out.println(sum);
    }
}