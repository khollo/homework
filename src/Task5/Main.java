package Task5;

import java.util.Scanner;

/**
 * Write an application that takes a positive number from the user (type int) and prints all
 * prime numbers greater than 1 and less than the given number.
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        loopPrime(input);
    }

    private static void loopPrime(int number) {
        for(int i = 2; i < number; i++) {
            if(isPrime(i)) {
                System.out.print(i + " ");
            }
        }
    }


    private static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        for (int i = 2; i <= (number / 2); i++) {
            if (number % i == 0) {
                return false;
            }
        } return true;
    }


}

