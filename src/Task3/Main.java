package Task3;

import java.util.Scanner;

/**
 * Write a program for solving a quadratic equation. The program should take three integers
 * (coefficients of the quadratic equation a, b, c) and calculate the roots of the
 * equation
 *  If delta ∆ comes out negative, print "Delta negative" and exit the program.
 * Formulas you will need
 *
 *  delta = b^2 - 4ac
 *  x1 = (-b - SQRT/Delta) / 2a
 *  x2 = (-b + SQRT/Delta) / 2a
 *
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("First number: ");
        int a = scanner.nextInt();
        System.out.println("Second number: ");
        int b = scanner.nextInt();
        System.out.println("Third number: ");
        int c = scanner.nextInt();
    quadraticEquation(a,b,c);
    }

    private static void quadraticEquation(int a, int b, int c) {
        double delta = Math.pow(b,2) - 4 * a * c;
        if (delta < 0) {
            System.out.println("Delta negative");
            return;
        }

        double rootOne = (-b + Math.sqrt(delta)) / (2 * a);
        double rootTwo = (-b - Math.sqrt(delta)) / (2 * a);

        System.out.println(rootOne);
        System.out.println(rootTwo);
    }
}
