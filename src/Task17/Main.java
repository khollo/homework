package Task17;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * Write an application that will read from the user the date of your next SDA classes and
 * calculate how many days are left to them.
 * Hint: read the date as String and parse it to LocalDate. Get the current date using
 * LocalDate.now() method.
 */


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        daysUntilNextClass(text);
    }

    private static void daysUntilNextClass(String text) {
        LocalDate nextClass = LocalDate.parse(text);
        LocalDate dateToday = LocalDate.now();
        System.out.println(nextClass.until(dateToday).getDays());
    }


}
