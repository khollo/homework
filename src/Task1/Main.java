package Task1;

import java.util.Scanner;

/**
 * Write an application that will read diameter of a circle (variable of type float) and
 * calculate perimeter of given circle.
 * Firstly, assume π = 3.14. Later, use value of π from built-in Math class.
 */

public class Main {
    public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    float diameterOfCircle = scanner.nextFloat();
    System.out.println(perimeterOfCircle(diameterOfCircle));

    }

    private static float perimeterOfCircle(float diameterOfCircle) {
        // Perimeter (P) = 2 · π · R
        // final double PI = 3.14;

        float radiusOfCircle = diameterOfCircle / 2;
        return (float) (2 * Math.PI * radiusOfCircle);
    }
}
