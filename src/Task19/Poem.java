package Task19;

public class Poem extends Author{
    private int stropheNumbers;

    public Poem(String surname, String nationality, int stropheNumbers) {
        super(surname, nationality);
        this.stropheNumbers = stropheNumbers;
    }


    public int getStropheNumbers() {
        return stropheNumbers;
    }

    public void setStropheNumbers(int stropheNumbers) {
        this.stropheNumbers = stropheNumbers;
    }


}
