package Task19;

import javax.xml.crypto.Data;
import java.util.Arrays;

/**
 * Write an application that consists of few classes:
 * a. Author class, representing an author – poem writer, which consists of fields surname
 * and nationality (both of type String)
 * b. Poem class, representing poem, which consists of fields creator (type Author) and
 * stropheNumbers (type int – numbers of strophes in poem)
 * c. Main class, with main method, inside which you will:
 * i. Create three instances of Poem class, fill them with data (using constructor
 * and/or setters) and store them in array
 * ii. Write a surname of an author, that wrote a longest poem (let your application
 * calculate it!)
 */

public class Main {
    public static void main(String[] args) {
        Poem firstPoem = new Poem("FirstPoem", "Estonian", 456);
        Poem secondPoem = new Poem("SecondPoem", "Finnish", 999);
        Poem thirdPoem = new Poem("ThirdPoem", "Russian", 53);

        longestPoem(firstPoem,secondPoem,thirdPoem);


    }

    private static void longestPoem(Poem firstPoem, Poem secondPoem, Poem thirdPoem) {

        Object[] list = {firstPoem, secondPoem, thirdPoem};
        if (firstPoem.getStropheNumbers() > secondPoem.getStropheNumbers() &&
                firstPoem.getStropheNumbers() > thirdPoem.getStropheNumbers())
            System.out.println("Longest poem is written by " + firstPoem.getSurname() +
                    " with " + firstPoem.getStropheNumbers() + " strophes!");

        if (secondPoem.getStropheNumbers() > firstPoem.getStropheNumbers() &&
                secondPoem.getStropheNumbers() > thirdPoem.getStropheNumbers())
            System.out.println("Longest poem is written by " + secondPoem.getSurname() +
                    " with " + secondPoem.getStropheNumbers() + " strophes!");

        if (thirdPoem.getStropheNumbers() > firstPoem.getStropheNumbers() &&
                thirdPoem.getStropheNumbers() > secondPoem.getStropheNumbers())
            System.out.println("Longest poem is written by " + thirdPoem.getSurname() +
                    " with " + thirdPoem.getStropheNumbers() + " strophes!");
    }
}


